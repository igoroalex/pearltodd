import uvicorn

from app.utils.config import settings


def main():
    uvicorn.run('app.main:app', host=settings.HOST, port=settings.PORT, reload=settings.RELOAD)


if __name__ == '__main__':
    main()
