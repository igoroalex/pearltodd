import pytest

import app.routers as api
from app.schemas.base_orm import ResponseDetail, ResponseDetailLoc
from app.schemas.master import Master
from tests.common import BasicForTest
from tests.conftest import client


@pytest.mark.order('first')
class TestMaster(BasicForTest):
    URL = f'{api.master_router.prefix}/'

    def test_post_master(self, header_master_1, header_master_2):
        response = client.post(
            url=self.URL,
            json={'telegram_id': "master_1", 'full_name': "master_1", 'username': "master_1"},
            headers=header_master_1,
        )
        assert response.status_code == 201, response.text
        rec = Master.parse_obj(response.json()).record
        assert rec.username == 'master_1'

        response = client.post(
            url=self.URL,
            json={'telegram_id': "master_2", 'full_name': "master_2", 'username': "master_2"},
            headers=header_master_2,
        )
        assert response.status_code == 201, response.text
        rec = Master.parse_obj(response.json()).record
        assert rec.username == 'master_2'

    def test_post_master_same(self, header_master_1):
        response = client.post(
            url=self.URL,
            json={'telegram_id': "master_1", 'full_name': "master_1", 'username': "master_1"},
            headers=header_master_1,
        )
        assert response.status_code == 404, response.text
        message = ResponseDetail.parse_obj(response.json())
        assert message.detail == "Cannot create record"

    def test_master_not_authenticated(self):
        response = client.post(
            url=self.URL,
            json={'telegram_id': "master_1", 'full_name': "master_1", 'username': "master_1"},
        )
        assert response.status_code == 401, response.text
        message = ResponseDetail.parse_obj(response.json())
        assert message.detail == "Not authenticated"

    def test_post_master_field_required(self, header_master_1):
        response = client.post(
            url=self.URL,
            json={'full_name': "master_1", 'username': "master_1"},
            headers=header_master_1,
        )
        assert response.status_code == 422, response.text
        message = ResponseDetailLoc.parse_obj(response.json())
        assert message.detail[0].msg == "field required"
        assert message.detail[0].loc[1] == "telegram_id"

        response = client.post(
            url=self.URL,
            json={'telegram_id': "master_1", 'username': "master_1"},
            headers=header_master_1,
        )
        assert response.status_code == 422, response.text
        message = ResponseDetailLoc.parse_obj(response.json())
        assert message.detail[0].msg == "field required"
        assert message.detail[0].loc[1] == "full_name"

        response = client.post(
            url=self.URL,
            json={'telegram_id': "master_1", 'full_name': "master_1"},
            headers=header_master_1,
        )
        assert response.status_code == 422, response.text
        message = ResponseDetailLoc.parse_obj(response.json())
        assert message.detail[0].msg == "field required"
        assert message.detail[0].loc[1] == "username"

    def test_get_master(self, header_master_1):
        response = client.get(
            url=f'{self.URL}master',
            headers=header_master_1,
        )
        assert response.status_code == 200, response.text
        record = Master.parse_obj(response.json()).record
        assert record.full_name == "master_1"
        assert record.username == "master_1"

    def test_get_master_not_authenticated(self):
        response = client.get(
            url=f'{self.URL}master',
        )
        assert response.status_code == 401, response.text
        message = ResponseDetail.parse_obj(response.json())
        assert message.detail == "Not authenticated"

        response = client.get(
            url=f'{self.URL}master',
            headers=self.get_header('test99'),
        )
        assert response.status_code == 401, response.text
        message = ResponseDetail.parse_obj(response.json())
        assert message.detail == "Not authenticated"
