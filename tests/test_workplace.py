import pytest

import app.routers as api
from app.schemas.base_orm import ResponseDetail, ResponseDetailLoc
from app.schemas.workplace import Workplace, Workplaces
from tests.common import BasicForTest
from tests.conftest import client


class TestWorkplace(BasicForTest):
    URL = f'{api.workplace_router.prefix}/'

    def test_get_no_workplaces(self, header_master_1):
        response = client.get(
            url=self.URL,
            headers=header_master_1,
        )
        assert response.status_code == 200, response.text
        records = Workplaces.parse_obj(response.json()).records
        assert len(records) == 0

    def test_post_workplace(self, header_master_1, header_master_2):
        response = client.post(
            url=self.URL,
            json={'address': "address_11"},
            headers=header_master_1,
        )
        assert response.status_code == 201, response.text
        rec = Workplace.parse_obj(response.json()).record
        assert rec.address == 'address_11'

        response = client.post(
            url=self.URL,
            json={'address': "address_12"},
            headers=header_master_1,
        )
        assert response.status_code == 201, response.text
        rec = Workplace.parse_obj(response.json()).record
        assert rec.address == 'address_12'

        response = client.post(
            url=self.URL,
            json={'address': "address_21"},
            headers=header_master_2,
        )
        assert response.status_code == 201, response.text
        rec = Workplace.parse_obj(response.json()).record
        assert rec.address == 'address_21'

        response = client.post(
            url=self.URL,
            json={'address': "address_22"},
            headers=header_master_2,
        )
        assert response.status_code == 201, response.text
        rec = Workplace.parse_obj(response.json()).record
        assert rec.address == 'address_22'

    def test_post_workplace_same(self, header_master_1):
        response = client.post(
            url=self.URL,
            json={'address': "address_11"},
            headers=header_master_1,
        )
        assert response.status_code == 404, response.text
        message = ResponseDetail.parse_obj(response.json())
        assert message.detail == "Cannot create record"

    def test_post_workplace_field_required(self, header_master_1):
        response = client.post(
            url=self.URL,
            headers=header_master_1,
        )
        assert response.status_code == 422, response.text
        message = ResponseDetailLoc.parse_obj(response.json())
        assert message.detail[0].msg == "field required"

        response = client.post(
            url=self.URL,
            json={'other_address': "master_1"},
            headers=header_master_1,
        )
        assert response.status_code == 422, response.text
        message = ResponseDetailLoc.parse_obj(response.json())
        assert message.detail[0].msg == "field required"
        assert message.detail[0].loc[1] == "address"

    def test_get_workplaces(self, header_master_1, header_master_2):
        response = client.get(
            url=self.URL,
            headers=header_master_1,
        )
        assert response.status_code == 200, response.text
        records = Workplaces.parse_obj(response.json()).records
        assert len(records) == 2
        assert records[0].address == "address_11"
        assert records[1].address == "address_12"

        response = client.get(
            url=self.URL,
            headers=header_master_2,
        )
        assert response.status_code == 200, response.text
        records = Workplaces.parse_obj(response.json()).records
        assert len(records) == 2
        assert records[0].address == "address_21"
        assert records[1].address == "address_22"

    def test_workplace_not_authenticated(self):
        self._not_authenticated()
