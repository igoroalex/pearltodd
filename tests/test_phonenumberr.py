import pytest

import app.routers as api
from app.schemas.base_orm import ResponseDetail, ResponseDetailLoc
from app.schemas.phone_number import PhoneNumber
from tests.common import BasicForTest
from tests.conftest import client


@pytest.mark.order('first')
class TestMaster(BasicForTest):
    URL = f'{api.phone_number_router.prefix}/'

    @pytest.mark.parametrize('phone_number', ['0689444888', '689444888', '380689444888', '=380689444888'])
    def test_get_phone_number(self, header_master_1, phone_number):
        response = client.get(
            url=f'{self.URL}',
            params={'phone_number': phone_number},
            headers=header_master_1,
        )
        assert response.status_code == 200, response.text
        record = PhoneNumber.parse_obj(response.json())
        assert record.phone_number == "+380689444888"

    @pytest.mark.parametrize('phone_number', ['068944488', '06894448888'])
    def test_get_phone_number_wrong(self, header_master_1, phone_number):
        response = client.get(
            url=f'{self.URL}',
            params={'phone_number': phone_number},
            headers=header_master_1,
        )
        assert response.status_code == 404, response.text
        message = ResponseDetail.parse_obj(response.json())
        assert message.detail == "phone number is not valid"

    def test_get_phone_number_field_required(self, header_master_1):
        response = client.get(
            url=f'{self.URL}',
            headers=header_master_1,
        )
        assert response.status_code == 422, response.text
        message = ResponseDetailLoc.parse_obj(response.json())
        assert message.detail[0].msg == "field required"
        assert message.detail[0].loc[1] == "phone_number"

    def test_get_phone_number_not_authenticated(self):
        response = client.get(
            url=f'{self.URL}',
            params={'phone_number': '0689444888'},
        )
        assert response.status_code == 401, response.text
        message = ResponseDetail.parse_obj(response.json())
        assert message.detail == "Not authenticated"

        response = client.get(
            url=f'{self.URL}',
            params={'phone_number': '0689444888'},
            headers=self.get_header('test99'),
        )
        assert response.status_code == 401, response.text
        message = ResponseDetail.parse_obj(response.json())
        assert message.detail == "Not authenticated"
