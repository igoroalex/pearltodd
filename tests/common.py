from datetime import date, timedelta

from app.schemas.base_orm import ResponseDetail
from app.schemas.token_data import Token
from tests.conftest import client


class BasicForTest:
    URL = 'test_url'
    TEST_DATE = date.today()
    TEST_DATE_2 = TEST_DATE + timedelta(days=1)

    @staticmethod
    def get_header(user_ext_id):
        token = Token(user_ext_id=user_ext_id, scope=['master'])
        return {'Authorization': f"Bearer {token.access_token}"}

    def _not_authenticated(self):
        response = client.post(
            url=self.URL,
        )
        assert response.status_code == 401, response.text
        message = ResponseDetail.parse_obj(response.json())
        assert message.detail == 'Not authenticated'

        response = client.post(
            url=self.URL,
            headers=self.get_header('test99'),
        )
        assert response.status_code == 401, response.text
        message = ResponseDetail.parse_obj(response.json())
        assert message.detail == 'Not authenticated'

        response = client.get(
            url=self.URL,
        )
        assert response.status_code == 401, response.text
        message = ResponseDetail.parse_obj(response.json())
        assert message.detail == 'Not authenticated'

        response = client.get(
            url=self.URL,
            headers=self.get_header('test99'),
        )
        assert response.status_code == 401, response.text
        message = ResponseDetail.parse_obj(response.json())
        assert message.detail == 'Not authenticated'

        response = client.delete(
            url=f'{self.URL}99',
        )
        assert response.status_code == 401, response.text
        message = ResponseDetail.parse_obj(response.json())
        assert message.detail == 'Not authenticated'

        response = client.delete(
            url=f'{self.URL}99',
            headers=self.get_header('test99'),
        )
        assert response.status_code == 401, response.text
        message = ResponseDetail.parse_obj(response.json())
        assert message.detail == 'Not authenticated'
