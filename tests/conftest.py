import pytest
from fastapi.testclient import TestClient

from app.utils import create_metadata, drop_metadata
import app.routers as api
from app.main import app, startup_event
from app.schemas.client import Clients
from app.schemas.schedule import Schedules
from app.schemas.service import Services
from app.schemas.token_data import Token
from app.schemas.workplace import Workplaces

drop_metadata()
create_metadata()

client = TestClient(app)


@pytest.fixture(scope='session', autouse=True)
def prepare_db():
    startup_event()
    create_metadata()
    yield
    drop_metadata()


@pytest.fixture
def header_master_1():
    token = Token(user_ext_id='master_1', scope=['master'])
    return {'Authorization': f"Bearer {token.access_token}"}


@pytest.fixture
def header_master_2():
    token = Token(user_ext_id='master_2', scope=['master'])
    return {'Authorization': f"Bearer {token.access_token}"}


@pytest.fixture
def header_master_99():
    token = Token(user_ext_id='master_99', scope=['master'])
    return {'Authorization': f"Bearer {token.access_token}"}


@pytest.fixture
def header_client():
    token = Token(user_ext_id='fox', scope=['client'])
    return {'Authorization': f"Bearer {token.access_token}"}


@pytest.fixture
def cur_workplace_1(header_master_1):
    response = client.get(
        url=f'{api.workplace_router.prefix}/',
        headers=header_master_1,
    )
    return Workplaces.parse_obj(response.json()).records[0]


@pytest.fixture
def cur_workplace_2(header_master_2):
    response = client.get(
        url=f'{api.workplace_router.prefix}/',
        headers=header_master_2,
    )
    return Workplaces.parse_obj(response.json()).records[0]


@pytest.fixture
def cur_schedule_1(header_master_1):
    response = client.get(
        url=f'{api.schedule_router.prefix}/',
        headers=header_master_1,
    )
    return Schedules.parse_obj(response.json()).records[0]


@pytest.fixture
def cur_schedule_2(header_master_2):
    response = client.get(
        url=f'{api.schedule_router.prefix}/',
        headers=header_master_2,
    )
    return Schedules.parse_obj(response.json()).records[0]


@pytest.fixture
def cur_client_1_1(header_master_1):
    response = client.get(
        url=f'{api.client_router.prefix}/',
        headers=header_master_1,
    )
    return Clients.parse_obj(response.json()).records[0]


@pytest.fixture
def cur_client_1_2(header_master_1):
    response = client.get(
        url=f'{api.client_router.prefix}/',
        headers=header_master_1,
    )
    return Clients.parse_obj(response.json()).records[1]


@pytest.fixture
def cur_client_2_1(header_master_2):
    response = client.get(
        url=f'{api.client_router.prefix}/',
        headers=header_master_2,
    )
    return Clients.parse_obj(response.json()).records[0]


@pytest.fixture
def cur_client_2_2(header_master_2):
    response = client.get(
        url=f'{api.client_router.prefix}/',
        headers=header_master_2,
    )
    return Clients.parse_obj(response.json()).records[1]


@pytest.fixture()
def cur_service_1_1(header_master_1):
    response = client.get(
        url=f'{api.service_router.prefix}/',
        headers=header_master_1,
    )
    return Services.parse_obj(response.json()).records[0]


@pytest.fixture
def cur_service_2_1(header_master_2):
    response = client.get(
        url=f'{api.service_router.prefix}/',
        headers=header_master_2,
    )
    return Services.parse_obj(response.json()).records[0]
