import pytest

import app.routers as api
from app.schemas.base_orm import ResponseDetail, ResponseDetailLoc
from app.schemas.client import Client, Clients
from tests.common import BasicForTest
from tests.conftest import client


class TestClient(BasicForTest):
    URL = f'{api.client_router.prefix}/'

    def test_get_no_clients_no_workplaces(self, header_master_1):
        response = client.get(
            url=self.URL,
            headers=header_master_1,
        )
        assert response.status_code == 200, response.text
        records = Clients.parse_obj(response.json()).records
        assert len(records) == 0

        response = client.get(
            url=self.URL,
            params={'workplace_id': 99},
            headers=header_master_1,
        )
        assert response.status_code == 200, response.text
        records = Clients.parse_obj(response.json()).records
        assert len(records) == 0

    @pytest.mark.order(after='test_workplace.py::TestWorkplace::test_post_workplace')
    def test_post_client(self, header_master_1, cur_workplace_1, cur_workplace_2):
        response = client.post(
            url=self.URL,
            json={
                'workplace_id': cur_workplace_1.id,
                'name': "Fox",
                'telegram_id': "fox",
                'phone_number': "789456",
            },
            headers=header_master_1,
        )
        assert response.status_code == 201, response.text
        rec = Client.parse_obj(response.json()).record
        assert rec.workplace_id == cur_workplace_1.id
        assert rec.name == 'Fox'
        assert rec.telegram_id == 'fox'
        assert rec.phone_number == '789456'

        response = client.post(
            url=self.URL,
            json={
                'workplace_id': cur_workplace_1.id,
                'name': "Dana",
                'telegram_id': "dana",
                'phone_number': "78945656",
            },
            headers=header_master_1,
        )
        assert response.status_code == 201, response.text
        rec = Client.parse_obj(response.json()).record
        assert rec.name == 'Dana'

        response = client.post(
            url=self.URL,
            json={
                'workplace_id': cur_workplace_2.id,
                'name': "Skinner",
                'telegram_id': "skinner",
                'phone_number': "789456",
            },
            headers=header_master_1,
        )
        assert response.status_code == 201, response.text
        rec = Client.parse_obj(response.json()).record
        assert rec.workplace_id == cur_workplace_2.id
        assert rec.name == 'Skinner'
        assert rec.telegram_id == 'skinner'
        assert rec.phone_number == '789456'

        response = client.post(
            url=self.URL,
            json={
                'workplace_id': cur_workplace_2.id,
                'name': "Dana",
                'telegram_id': "dana",
                'phone_number': "78945656",
            },
            headers=header_master_1,
        )
        assert response.status_code == 201, response.text
        rec = Client.parse_obj(response.json()).record
        assert rec.name == 'Dana'

    @pytest.mark.order(after='test_workplace.py::TestWorkplace::test_post_workplace')
    def test_post_client_same(self, header_master_1, cur_workplace_1):
        response = client.post(
            url=self.URL,
            json={
                'workplace_id': cur_workplace_1.id,
                'name': "Fox",
                'telegram_id': "fox",
                'phone_number': "789456",
            },
            headers=header_master_1,
        )
        assert response.status_code == 404, response.text
        message = ResponseDetail.parse_obj(response.json())
        assert message.detail == "Cannot create record"

    @pytest.mark.order(after='test_workplace.py::TestWorkplace::test_post_workplace')
    def test_post_client_not_authenticated(self, cur_workplace_1):
        response = client.post(
            url=self.URL,
        )
        assert response.status_code == 401, response.text
        message = ResponseDetail.parse_obj(response.json())
        assert message.detail == "Not authenticated"

        response = client.post(
            url=self.URL,
            headers=self.get_header('test99'),
        )
        assert response.status_code == 401, response.text
        message = ResponseDetail.parse_obj(response.json())
        assert message.detail == "Not authenticated"

    @pytest.mark.order(after='test_client.py::TestClient::test_post_client')
    def test_post_client_relationship(self, header_master_1):
        response = client.post(
            url=self.URL,
            json={
                'workplace_id': 99,
                'name': "Fox",
                'telegram_id': "fox",
                'phone_number': "789456",
            },
            headers=header_master_1,
        )
        assert response.status_code == 404, response.text
        message = ResponseDetail.parse_obj(response.json())
        assert message.detail == "No workplace found"

    @pytest.mark.order(after='test_service.py::TestService::test_post_service')
    def test_post_client_field_required(self, header_master_1, cur_workplace_1):
        response = client.post(
            url=self.URL,
            json={
                'name': "Fox",
                'telegram_id': "fox",
                'phone_number': "789456",
            },
            headers=header_master_1,
        )
        assert response.status_code == 422, response.text
        message = ResponseDetailLoc.parse_obj(response.json())
        assert message.detail[0].msg == "field required"
        assert message.detail[0].loc[1] == "workplace_id"

        response = client.post(
            url=self.URL,
            json={
                'workplace_id': cur_workplace_1.id,
                'telegram_id': "fox",
                'phone_number': "789456",
            },
            headers=header_master_1,
        )
        assert response.status_code == 422, response.text
        message = ResponseDetailLoc.parse_obj(response.json())
        assert message.detail[0].msg == "field required"
        assert message.detail[0].loc[1] == "name"

        response = client.post(
            url=self.URL,
            json={
                'workplace_id': cur_workplace_1.id,
                'name': "Fox",
                'phone_number': "789456",
            },
            headers=header_master_1,
        )
        assert response.status_code == 422, response.text
        message = ResponseDetailLoc.parse_obj(response.json())
        assert message.detail[0].msg == "field required"
        assert message.detail[0].loc[1] == "telegram_id"

    @pytest.mark.order(after='test_client.py::TestClient::test_post_client')
    def test_get_clients(self, header_master_1, header_master_2):
        response = client.get(
            url=self.URL,
            headers=header_master_1,
        )
        assert response.status_code == 200, response.text
        records = Clients.parse_obj(response.json()).records
        assert len(records) == 2
        assert records[0].name == "Fox"
        assert records[1].name == "Dana"

        response = client.get(
            url=self.URL,
            headers=header_master_2,
        )
        assert response.status_code == 200, response.text
        records = Clients.parse_obj(response.json()).records
        assert len(records) == 2
        assert records[0].name == "Skinner"
        assert records[1].name == "Dana"

        response = client.get(
            url=self.URL,
            params={'search_data': 'John'},
            headers=header_master_1,
        )
        assert response.status_code == 200, response.text
        records = Clients.parse_obj(response.json()).records
        assert len(records) == 0

        response = client.get(
            url=self.URL,
            params={'search_data': 'Dan'},
            headers=header_master_1,
        )
        assert response.status_code == 200, response.text
        records = Clients.parse_obj(response.json()).records
        assert len(records) == 1

    @pytest.mark.order(after='test_client.py::TestClient::test_post_client')
    def test_get_services_workplace(self, header_master_1, cur_workplace_1):
        response = client.get(
            url=self.URL,
            params={'workplace_id': cur_workplace_1.id},
            headers=header_master_1,
        )
        assert response.status_code == 200, response.text
        records = Clients.parse_obj(response.json()).records
        assert len(records) == 2

    def test_clients_not_authenticated(self):
        self._not_authenticated()
