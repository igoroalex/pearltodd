from datetime import datetime, time

import pytest

import app.routers as api
from app.schemas.base_orm import ResponseDetail, ResponseDetailLoc
from app.schemas.booking import Booking, Bookings
from tests.common import BasicForTest
from tests.conftest import client


@pytest.mark.order(after='test_service.py::TestService::test_post_service')
class TestBooking(BasicForTest):
    URL = f'{api.booking_router.prefix}/'

    def test_post_booking_1(
        self,
        header_master_1,
        cur_workplace_1,
        cur_client_1_1,
        cur_client_1_2,
        cur_service_1_1,
    ):
        booking_datetime = datetime.combine(self.TEST_DATE, time(10, 0, 0))
        response = client.post(
            url=self.URL,
            json={
                'workplace_id': cur_workplace_1.id,
                'client_id': cur_client_1_1.id,
                'service_id': cur_service_1_1.id,
                'start_datetime': booking_datetime.isoformat(),
            },
            headers=header_master_1,
        )
        assert response.status_code == 201, response.text
        rec = Booking.parse_obj(response.json()).record
        assert rec.workplace_id == cur_workplace_1.id
        assert rec.client_id == cur_client_1_1.id
        assert rec.service_id == cur_service_1_1.id
        assert rec.start_datetime == booking_datetime

        booking_datetime = datetime.combine(self.TEST_DATE, time(11, 0, 0))
        response = client.post(
            url=self.URL,
            json={
                'workplace_id': cur_workplace_1.id,
                'client_id': cur_client_1_2.id,
                'service_id': cur_service_1_1.id,
                'start_datetime': booking_datetime.isoformat(),
            },
            headers=header_master_1,
        )
        assert response.status_code == 201, response.text
        rec = Booking.parse_obj(response.json()).record
        assert rec.start_datetime == booking_datetime

    def test_post_booking_2(
        self,
        header_master_1,
        cur_workplace_2,
        cur_client_2_1,
        cur_client_2_2,
        cur_service_2_1,
    ):
        booking_datetime = datetime.combine(self.TEST_DATE, time(10, 0, 0))
        response = client.post(
            url=self.URL,
            json={
                'workplace_id': cur_workplace_2.id,
                'client_id': cur_client_2_1.id,
                'service_id': cur_service_2_1.id,
                'start_datetime': booking_datetime.isoformat(),
            },
            headers=header_master_1,
        )
        assert response.status_code == 201, response.text
        rec = Booking.parse_obj(response.json()).record
        assert rec.workplace_id == cur_workplace_2.id
        assert rec.client_id == cur_client_2_1.id
        assert rec.service_id == cur_service_2_1.id
        assert rec.start_datetime == booking_datetime

        booking_datetime = datetime.combine(self.TEST_DATE, time(11, 0, 0))
        response = client.post(
            url=self.URL,
            json={
                'workplace_id': cur_workplace_2.id,
                'client_id': cur_client_2_2.id,
                'service_id': cur_service_2_1.id,
                'start_datetime': booking_datetime.isoformat(),
            },
            headers=header_master_1,
        )
        assert response.status_code == 201, response.text
        rec = Booking.parse_obj(response.json()).record
        assert rec.start_datetime == booking_datetime

    def test_post_booking_same(
        self,
        header_master_1,
        cur_workplace_1,
        cur_client_1_1,
        cur_client_1_2,
        cur_service_1_1,
    ):
        booking_datetime = datetime.combine(self.TEST_DATE, time(10, 0, 0))
        response = client.post(
            url=self.URL,
            json={
                'workplace_id': cur_workplace_1.id,
                'client_id': cur_client_1_1.id,
                'service_id': cur_service_1_1.id,
                'start_datetime': booking_datetime.isoformat(),
            },
            headers=header_master_1,
        )
        assert response.status_code == 404, response.text
        message = ResponseDetail.parse_obj(response.json())
        assert message.detail == "Cannot create record"

    def test_post_booking_relationship(
        self,
        header_master_1,
        cur_workplace_1,
        cur_client_1_1,
        cur_client_1_2,
        cur_service_1_1,
    ):
        booking_datetime = datetime.combine(self.TEST_DATE, time(12, 0, 0))
        response = client.post(
            url=self.URL,
            json={
                'workplace_id': 99,
                'client_id': cur_client_1_1.id,
                'service_id': cur_service_1_1.id,
                'start_datetime': booking_datetime.isoformat(),
            },
            headers=header_master_1,
        )
        assert response.status_code == 404, response.text
        message = ResponseDetail.parse_obj(response.json())
        assert message.detail == "No workplace found"

        # TODO poltergeist
        # response = client.post(
        #     url=self.URL,
        #     json={
        #         'workplace_id': cur_workplace_1.id,
        #         'client_id': 99,
        #         'service_id': cur_service_1_1.id,
        #         'start_datetime': booking_datetime.isoformat(),
        #     },
        #     headers=header_master_1,
        # )
        # assert response.status_code == 404, response.text
        # message = ResponseDetail.parse_obj(response.json())
        # assert message.detail == "No client found"
        #
        # response = client.get(
        #     url=self.URL,
        #     headers=header_master_1,
        # )
        # assert response.status_code == 200, response.text
        # records = Bookings.parse_obj(response.json()).records
        #
        # response = client.post(
        #     url=self.URL,
        #     json={
        #         'workplace_id': cur_workplace_1.id,
        #         'client_id': cur_client_1_1.id,
        #         'service_id': 99,
        #         'start_datetime': booking_datetime.isoformat(),
        #     },
        #     headers=header_master_1,
        # )
        # assert response.status_code == 404, response.text
        # message = ResponseDetail.parse_obj(response.json())
        # assert message.detail == "No service found"

    def test_post_booking_field_required(
        self,
        header_master_1,
        cur_workplace_1,
        cur_client_1_1,
        cur_client_1_2,
        cur_service_1_1,
    ):
        booking_datetime = datetime.combine(self.TEST_DATE, time(11, 0, 0))
        response = client.post(
            url=self.URL,
            json={
                'client_id': cur_client_1_1.id,
                'service_id': cur_service_1_1.id,
                'start_datetime': booking_datetime.isoformat(),
            },
            headers=header_master_1,
        )
        assert response.status_code == 422, response.text
        message = ResponseDetailLoc.parse_obj(response.json())
        assert message.detail[0].msg == 'field required'
        assert message.detail[0].loc[1] == "workplace_id"

        response = client.post(
            url=self.URL,
            json={
                'workplace_id': cur_workplace_1.id,
                'service_id': cur_service_1_1.id,
                'start_datetime': booking_datetime.isoformat(),
            },
            headers=header_master_1,
        )
        assert response.status_code == 422, response.text
        message = ResponseDetailLoc.parse_obj(response.json())
        assert message.detail[0].msg == 'field required'
        assert message.detail[0].loc[1] == "client_id"

        response = client.post(
            url=self.URL,
            json={
                'workplace_id': cur_workplace_1.id,
                'client_id': cur_client_1_1.id,
                'start_datetime': booking_datetime.isoformat(),
            },
            headers=header_master_1,
        )
        assert response.status_code == 422, response.text
        message = ResponseDetailLoc.parse_obj(response.json())
        assert message.detail[0].msg == 'field required'
        assert message.detail[0].loc[1] == "service_id"

        response = client.post(
            url=self.URL,
            json={
                'workplace_id': cur_workplace_1.id,
                'client_id': cur_client_1_1.id,
                'service_id': cur_service_1_1.id,
            },
            headers=header_master_1,
        )
        assert response.status_code == 422, response.text
        message = ResponseDetailLoc.parse_obj(response.json())
        assert message.detail[0].msg == 'field required'
        assert message.detail[0].loc[1] == "start_datetime"

    def test_get_bookings(self, header_master_1, header_master_2):
        response = client.get(
            url=self.URL,
            headers=header_master_1,
        )
        assert response.status_code == 200, response.text
        records = Bookings.parse_obj(response.json()).records
        assert len(records) == 2
        assert records[0].start_datetime == datetime.combine(self.TEST_DATE, time(10, 0, 0))
        assert records[1].start_datetime == datetime.combine(self.TEST_DATE, time(11, 0, 0))

        response = client.get(
            url=self.URL,
            headers=header_master_2,
        )
        assert response.status_code == 200, response.text
        records = Bookings.parse_obj(response.json()).records
        assert len(records) == 2
        assert records[0].start_datetime == datetime.combine(self.TEST_DATE, time(10, 0, 0))
        assert records[1].start_datetime == datetime.combine(self.TEST_DATE, time(11, 0, 0))

    def test_booking_not_authenticated(self):
        self._not_authenticated()
