import pytest

import app.routers as api
from app.schemas.base_orm import ResponseDetail, ResponseDetailLoc
from app.schemas.work_date import WorkDate, WorkDates
from tests.common import BasicForTest
from tests.conftest import client


@pytest.mark.order(after='test_workplace.py::TestWorkplace::test_post_workplace')
class TestWorkDate(BasicForTest):
    URL = f'{api.work_date_router.prefix}/'

    def test_post_work_date(
        self,
        header_master_1,
        header_master_2,
        cur_workplace_1,
        cur_schedule_1,
        cur_workplace_2,
        cur_schedule_2,
    ):
        response = client.post(
            url=self.URL,
            json={
                'workplace_id': cur_workplace_1.id,
                'schedule_id': cur_schedule_1.id,
                'date': self.TEST_DATE.isoformat(),
            },
            headers=header_master_1,
        )
        assert response.status_code == 201, response.text
        rec = WorkDate.parse_obj(response.json()).record
        assert rec.date == self.TEST_DATE

        response = client.post(
            url=self.URL,
            json={
                'workplace_id': cur_workplace_1.id,
                'schedule_id': cur_schedule_1.id,
                'date': self.TEST_DATE_2.isoformat(),
            },
            headers=header_master_1,
        )
        assert response.status_code == 201, response.text
        rec = WorkDate.parse_obj(response.json()).record
        assert rec.date == self.TEST_DATE_2

        response = client.post(
            url=self.URL,
            json={
                'workplace_id': cur_workplace_2.id,
                'schedule_id': cur_schedule_2.id,
                'date': self.TEST_DATE.isoformat(),
            },
            headers=header_master_2,
        )
        assert response.status_code == 201, response.text
        rec = WorkDate.parse_obj(response.json()).record
        assert rec.date == self.TEST_DATE

        response = client.post(
            url=self.URL,
            json={
                'workplace_id': cur_workplace_2.id,
                'schedule_id': cur_schedule_2.id,
                'date': self.TEST_DATE_2.isoformat(),
            },
            headers=header_master_2,
        )
        assert response.status_code == 201, response.text
        rec = WorkDate.parse_obj(response.json()).record
        assert rec.date == self.TEST_DATE_2

    def test_post_work_date_same(self, header_master_1, cur_workplace_1, cur_schedule_1):
        response = client.post(
            url=self.URL,
            json={
                'workplace_id': cur_workplace_1.id,
                'schedule_id': cur_schedule_1.id,
                'date': self.TEST_DATE.isoformat(),
            },
            headers=header_master_1,
        )
        assert response.status_code == 201, response.text
        rec = WorkDate.parse_obj(response.json()).record
        assert rec.date == self.TEST_DATE

    def test_post_service_relationship(self, header_master_1, cur_schedule_1):
        response = client.post(
            url=self.URL,
            json={
                'workplace_id': 99,
                'schedule_id': cur_schedule_1.id,
                'date': self.TEST_DATE.isoformat(),
            },
            headers=header_master_1,
        )
        assert response.status_code == 404, response.text
        message = ResponseDetail.parse_obj(response.json())
        assert message.detail == "No workplace found"

        # TODO poltergeist
        # response = client.post(
        #     url=self.URL,
        #     json={
        #         'workplace_id': workplace.id,
        #         'schedule_id': 99,
        #         'date': self.TEST_DATE_2.isoformat(),
        #     },
        #     headers=header_master_1,
        # )
        # assert response.status_code == 404, response.text
        # message = ResponseDetail.parse_obj(response.json())
        # assert message.detail == "No schedule found"

        response = client.get(
            url=self.URL,
            headers=header_master_1,
        )
        assert response.status_code == 200, response.text
        records = WorkDates.parse_obj(response.json()).records
        assert len(records) == 2

    def test_post_service_field_required(self, header_master_1, cur_workplace_1, cur_schedule_1):
        response = client.post(
            url=self.URL,
            json={
                'workplace_id': cur_workplace_1.id,
                'schedule_id': cur_schedule_1.id,
            },
            headers=header_master_1,
        )
        assert response.status_code == 422, response.text
        message = ResponseDetailLoc.parse_obj(response.json())
        assert message.detail[0].msg == 'field required'
        assert message.detail[0].loc[1] == "date"

        response = client.post(
            url=self.URL,
            json={
                'workplace_id': cur_workplace_1.id,
                'date': self.TEST_DATE.isoformat(),
            },
            headers=header_master_1,
        )
        assert response.status_code == 422, response.text
        message = ResponseDetailLoc.parse_obj(response.json())
        assert message.detail[0].msg == 'field required'
        assert message.detail[0].loc[1] == "schedule_id"

        response = client.post(
            url=self.URL,
            json={
                'schedule_id': cur_schedule_1.id,
                'date': self.TEST_DATE.isoformat(),
            },
            headers=header_master_1,
        )
        assert response.status_code == 422, response.text
        message = ResponseDetailLoc.parse_obj(response.json())
        assert message.detail[0].msg == 'field required'
        assert message.detail[0].loc[1] == "workplace_id"

    def test_delete_work_date(self, header_master_1, cur_workplace_1, cur_schedule_1):
        response = client.get(
            url=self.URL,
            headers=header_master_1,
        )
        assert response.status_code == 200, response.text
        records = WorkDates.parse_obj(response.json()).records
        assert len(records) == 2
        assert records[0].date == self.TEST_DATE

        response = client.delete(
            url=f'{self.URL}{records[0].id}',
            headers=header_master_1,
        )
        assert response.status_code == 200, response.text
        rec = WorkDate.parse_obj(response.json()).record
        assert rec.date == self.TEST_DATE
        assert not rec.is_active

        response = client.get(
            url=self.URL,
            headers=header_master_1,
        )
        assert response.status_code == 200, response.text
        records = WorkDates.parse_obj(response.json()).records
        assert len(records) == 1
        assert records[0].date == self.TEST_DATE_2

        response = client.post(
            url=self.URL,
            json={
                'workplace_id': cur_workplace_1.id,
                'schedule_id': cur_schedule_1.id,
                'date': self.TEST_DATE.isoformat(),
            },
            headers=header_master_1,
        )
        assert response.status_code == 201, response.text
        rec = WorkDate.parse_obj(response.json()).record
        assert rec.date == self.TEST_DATE
        assert rec.is_active

        response = client.get(
            url=self.URL,
            headers=header_master_1,
        )
        assert response.status_code == 200, response.text
        records = WorkDates.parse_obj(response.json()).records
        assert len(records) == 2
        assert records[0].date == self.TEST_DATE
        assert records[1].date == self.TEST_DATE_2

    def test_delete_work_date_no(self, header_master_1):
        response = client.delete(
            url=f'{self.URL}99',
            headers=header_master_1,
        )
        assert response.status_code == 404, response.text
        message = ResponseDetail.parse_obj(response.json())
        assert message.detail == "No work_date found"

    def test_get_work_dates(self, header_master_1, header_master_2):
        response = client.get(
            url=self.URL,
            headers=header_master_1,
        )
        assert response.status_code == 200, response.text
        records = WorkDates.parse_obj(response.json()).records
        assert len(records) == 2
        assert records[0].date == self.TEST_DATE
        assert records[1].date == self.TEST_DATE_2

        response = client.get(
            url=self.URL,
            headers=header_master_2,
        )
        assert response.status_code == 200, response.text
        records = WorkDates.parse_obj(response.json()).records
        assert len(records) == 2
        assert records[0].date == self.TEST_DATE
        assert records[1].date == self.TEST_DATE_2

    def test_get_services_workplace(self, header_master_1, cur_workplace_1):
        response = client.get(
            url=self.URL,
            params={'workplace_id': cur_workplace_1.id},
            headers=header_master_1,
        )
        assert response.status_code == 200, response.text
        records = WorkDates.parse_obj(response.json()).records
        assert len(records) == 2
        assert records[0].date == self.TEST_DATE
        assert records[1].date == self.TEST_DATE_2

    def test_service_not_authenticated(self):
        self._not_authenticated()
