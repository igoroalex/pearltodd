from datetime import time

import app.routers as api
from app.schemas.base_orm import ResponseDetail, ResponseDetailLoc
from app.schemas.schedule import Schedule, Schedules
from tests.common import BasicForTest
from tests.conftest import client


class TestSchedule(BasicForTest):
    URL = f'{api.schedule_router.prefix}/'

    def test_get_no_schedules(self, header_master_1):
        response = client.get(
            url=self.URL,
            headers=header_master_1,
        )
        assert response.status_code == 200, response.text
        records = Schedules.parse_obj(response.json()).records
        assert len(records) == 0

    def test_post_schedule(self, header_master_1, header_master_2):
        response = client.post(
            url=self.URL,
            json={'start_time': "08:00", 'end_time': "19:00"},
            headers=header_master_1,
        )
        assert response.status_code == 201, response.text
        rec = Schedule.parse_obj(response.json()).record
        assert rec.start_time == time(hour=8, minute=0)
        assert rec.end_time == time(hour=19, minute=0)

        response = client.post(
            url=self.URL,
            json={'start_time': "10:30", 'end_time': "18:25"},
            headers=header_master_1,
        )
        assert response.status_code == 201, response.text
        rec = Schedule.parse_obj(response.json()).record
        assert rec.start_time == time(hour=10, minute=30)
        assert rec.end_time == time(hour=18, minute=25)

        response = client.post(
            url=self.URL,
            json={'start_time': "08:00", 'end_time': "19:00"},
            headers=header_master_2,
        )
        assert response.status_code == 201, response.text
        rec = Schedule.parse_obj(response.json()).record
        assert rec.start_time == time(hour=8, minute=0)
        assert rec.end_time == time(hour=19, minute=0)

        response = client.post(
            url=self.URL,
            json={'start_time': "10:30", 'end_time': "18:25"},
            headers=header_master_2,
        )
        assert response.status_code == 201, response.text
        rec = Schedule.parse_obj(response.json()).record
        assert rec.start_time == time(hour=10, minute=30)
        assert rec.end_time == time(hour=18, minute=25)

    def test_post_schedule_same(self, header_master_2):
        response = client.post(
            url=self.URL,
            json={'start_time': "08:00", 'end_time': "19:00"},
            headers=header_master_2,
        )
        assert response.status_code == 404, response.text
        message = ResponseDetail.parse_obj(response.json())
        assert message.detail == "Cannot create record"

    def test_post_schedule_field_required(self, header_master_1):
        response = client.post(
            url=self.URL,
            json={'end_time': "19:00"},
            headers=header_master_1,
        )
        assert response.status_code == 422, response.text
        message = ResponseDetailLoc.parse_obj(response.json())
        assert message.detail[0].msg == "field required"

        response = client.post(
            url=self.URL,
            json={'start_time': "08:00"},
            headers=header_master_1,
        )
        assert response.status_code == 422, response.text
        message = ResponseDetailLoc.parse_obj(response.json())
        assert message.detail[0].msg == "field required"

    def test_get_schedules(self, header_master_1, header_master_2):
        response = client.get(
            url=self.URL,
            headers=header_master_1,
        )
        assert response.status_code == 200, response.text
        records = Schedules.parse_obj(response.json()).records
        assert len(records) == 2
        assert records[0].start_time == time(hour=8, minute=0)
        assert records[0].end_time == time(hour=19, minute=0)
        assert records[1].start_time == time(hour=10, minute=30)
        assert records[1].end_time == time(hour=18, minute=25)

        response = client.get(
            url=self.URL,
            headers=header_master_2,
        )
        assert response.status_code == 200, response.text
        records = Schedules.parse_obj(response.json()).records
        assert len(records) == 2
        assert records[0].start_time == time(hour=8, minute=0)
        assert records[0].end_time == time(hour=19, minute=0)
        assert records[1].start_time == time(hour=10, minute=30)
        assert records[1].end_time == time(hour=18, minute=25)

    def test_schedule_not_authenticated(self):
        self._not_authenticated()
