import pytest

import app.routers as api
from app.schemas.base_orm import ResponseDetail, ResponseDetailLoc
from app.schemas.service import Service, Services
from tests.common import BasicForTest
from tests.conftest import client


class TestService(BasicForTest):
    URL = f'{api.service_router.prefix}/'

    def test_get_no_services_no_workplaces(self, header_master_1):
        response = client.get(
            url=self.URL,
            headers=header_master_1,
        )
        assert response.status_code == 200, response.text
        records = Services.parse_obj(response.json()).records
        assert len(records) == 0

        response = client.get(
            url=self.URL,
            params={'workplace_id': 99},
            headers=header_master_1,
        )
        assert response.status_code == 200, response.text
        records = Services.parse_obj(response.json()).records
        assert len(records) == 0

    @pytest.mark.order(after='test_workplace.py::TestWorkplace::test_post_workplace')
    def test_post_service(self, header_master_1, header_master_2, cur_workplace_1, cur_workplace_2):
        response = client.post(
            url=self.URL,
            json={
                'workplace_id': cur_workplace_1.id,
                'name': 'cut hair',
                'duration': 45,
                'price': 200,
                'rest_time': 10,
                'description': 'cut hair',
            },
            headers=header_master_1,
        )
        assert response.status_code == 201, response.text
        rec = Service.parse_obj(response.json()).record
        assert rec.name == 'cut hair'
        assert rec.price == 200

        response = client.post(
            url=self.URL,
            json={
                'workplace_id': cur_workplace_1.id,
                'name': 'beard cut',
                'duration': 45,
                'price': 300,
                'rest_time': 10,
                'description': 'beard hair',
            },
            headers=header_master_1,
        )
        assert response.status_code == 201, response.text
        rec = Service.parse_obj(response.json()).record
        assert rec.name == 'beard cut'
        assert rec.price == 300

        response = client.post(
            url=self.URL,
            json={
                'workplace_id': cur_workplace_2.id,
                'name': 'cut hair',
                'duration': 45,
                'price': 400,
                'rest_time': 10,
                'description': 'cut hair',
            },
            headers=header_master_2,
        )
        assert response.status_code == 201, response.text
        rec = Service.parse_obj(response.json()).record
        assert rec.name == 'cut hair'
        assert rec.price == 400

        response = client.post(
            url=self.URL,
            json={
                'workplace_id': cur_workplace_2.id,
                'name': 'cut beard',
                'duration': 45,
                'price': 500,
                'rest_time': 10,
                'description': 'cut hair',
            },
            headers=header_master_2,
        )
        assert response.status_code == 201, response.text
        rec = Service.parse_obj(response.json()).record
        assert rec.name == 'cut beard'
        assert rec.price == 500

    @pytest.mark.order(after='test_service.py::TestService::test_post_service')
    def test_post_service_same(self, header_master_1, cur_workplace_1):
        response = client.post(
            url=self.URL,
            json={
                'workplace_id': cur_workplace_1.id,
                'name': 'cut hair',
                'duration': 45,
                'price': 200,
                'rest_time': 10,
                'description': 'cut hair',
            },
            headers=header_master_1,
        )
        assert response.status_code == 404, response.text
        message = ResponseDetail.parse_obj(response.json())
        assert message.detail == "Cannot create record"

    def test_post_service_relationship(self, header_master_1):
        response = client.post(
            url=self.URL,
            json={
                'workplace_id': 99,
                'name': 'cut hair',
                'duration': 45,
                'price': 200,
                'rest_time': 10,
                'description': 'cut hair',
            },
            headers=header_master_1,
        )
        assert response.status_code == 404, response.text
        message = ResponseDetail.parse_obj(response.json())
        assert message.detail == "No workplace found"

    @pytest.mark.order(after='test_service.py::TestService::test_post_service')
    def test_post_service_field_required(self, header_master_1, cur_workplace_1):
        response = client.post(
            url=self.URL,
            json={
                'name': 'cut hair',
                'duration': 45,
                'price': 200,
                'rest_time': 10,
                'description': 'cut hair',
            },
            headers=header_master_1,
        )
        assert response.status_code == 422, response.text
        message = ResponseDetailLoc.parse_obj(response.json())
        assert message.detail[0].msg == "field required"
        assert message.detail[0].loc[1] == "workplace_id"

        response = client.post(
            url=self.URL,
            json={
                'workplace_id': cur_workplace_1.id,
                'duration': 45,
                'price': 200,
                'rest_time': 10,
                'description': 'cut hair',
            },
            headers=header_master_1,
        )
        assert response.status_code == 422, response.text
        message = ResponseDetailLoc.parse_obj(response.json())
        assert message.detail[0].msg == "field required"
        assert message.detail[0].loc[1] == "name"

        response = client.post(
            url=self.URL,
            json={
                'workplace_id': cur_workplace_1.id,
                'name': 'cut hair',
                'price': 200,
                'rest_time': 10,
                'description': 'cut hair',
            },
            headers=header_master_1,
        )
        assert response.status_code == 422, response.text
        message = ResponseDetailLoc.parse_obj(response.json())
        assert message.detail[0].msg == "field required"
        assert message.detail[0].loc[1] == "duration"

        response = client.post(
            url=self.URL,
            json={
                'workplace_id': cur_workplace_1.id,
                'name': 'cut hair',
                'duration': 45,
                'rest_time': 10,
                'description': 'cut hair',
            },
            headers=header_master_1,
        )
        assert response.status_code == 422, response.text
        message = ResponseDetailLoc.parse_obj(response.json())
        assert message.detail[0].msg == "field required"
        assert message.detail[0].loc[1] == "price"

    @pytest.mark.order(after='test_service.py::TestService::test_post_service')
    def test_get_services(self, header_master_1, header_master_2):
        response = client.get(
            url=self.URL,
            headers=header_master_1,
        )
        assert response.status_code == 200, response.text
        records = Services.parse_obj(response.json()).records
        assert len(records) == 2
        assert records[0].name == 'cut hair'
        assert records[0].price == 200
        assert records[1].name == 'beard cut'
        assert records[1].price == 300

        response = client.get(
            url=self.URL,
            headers=header_master_2,
        )
        assert response.status_code == 200, response.text
        records = Services.parse_obj(response.json()).records
        assert len(records) == 2
        assert records[0].name == 'cut hair'
        assert records[0].price == 400
        assert records[1].name == 'cut beard'
        assert records[1].price == 500

    @pytest.mark.order(after='test_service.py::TestService::test_post_service')
    def test_get_services_workplace(self, header_master_1, cur_workplace_1):
        response = client.get(
            url=self.URL,
            params={'workplace_id': cur_workplace_1.id},
            headers=header_master_1,
        )
        assert response.status_code == 200, response.text
        records = Services.parse_obj(response.json()).records
        assert len(records) == 2
        assert records[1].name == 'beard cut'
        assert records[1].price == 300

    @pytest.mark.order(after='test_service.py::TestService::test_post_service')
    def test_services_not_authenticated(self):
        self._not_authenticated()

    @pytest.mark.order(after='test_service.py::TestService::test_post_service')
    def test_patch_service(self, header_master_1, header_master_2, cur_service_1_1, cur_service_2_1):
        response = client.patch(
            url=f'{self.URL}{cur_service_1_1.id}',
            json={'price': 300},
            headers=header_master_1,
        )
        assert response.status_code == 200, response.json()
        record = Service.parse_obj(response.json()).record
        assert record.price == 300

        response = client.patch(
            url=f'{self.URL}{cur_service_1_1.id}',
            json={'duration': 60},
            headers=header_master_1,
        )
        assert response.status_code == 200, response.json()
        record = Service.parse_obj(response.json()).record
        assert record.duration == 60

        response = client.patch(
            url=f'{self.URL}{cur_service_1_1.id}',
            json={'duration': 90, 'rest_time': 20},
            headers=header_master_1,
        )
        assert response.status_code == 200, response.json()
        record = Service.parse_obj(response.json()).record
        assert record.duration == 90
        assert record.rest_time == 20

        response = client.patch(
            url=f'{self.URL}{cur_service_2_1.id}',
            json={'price': 0},
            headers=header_master_2,
        )
        assert response.status_code == 200, response.json()
        record = Service.parse_obj(response.json()).record
        assert record.price == cur_service_2_1.price

    @pytest.mark.order(after='test_service.py::TestService::test_post_service')
    def test_delete_service(self, header_master_1, header_master_2, cur_service_1_1):
        response = client.delete(
            url=f'{self.URL}{cur_service_1_1.id}',
            headers=header_master_2,
        )
        assert response.status_code == 404
        message = ResponseDetail.parse_obj(response.json())
        assert message.detail == "No service found"

        response = client.delete(
            url=f'{self.URL}{cur_service_1_1.id}',
            headers=header_master_1,
        )
        assert response.status_code == 200, response.text
        rec = Service.parse_obj(response.json()).record
        assert rec.id == cur_service_1_1.id
        assert not rec.is_active

    # @pytest.mark.order(after='test_service.py::TestService::test_post_service')
    # @pytest.mark.order(after='test_workplace.py::TestClient::test_post_client')
    # def test_services_scope_client(self, header_client):
    #     response = client.get(
    #         url=self.URL,
    #         headers=header_client,
    #     )
    #     assert response.status_code == 200, response.text
    #     records = Services.parse_obj(response.json()).records
    #     assert len(records) == 2
    #     assert records[0].name == 'cut hair'
    #     assert records[0].price == 200
    #     assert records[1].name == 'beard cut'
    #     assert records[1].price == 300
    #
    #     response = client.get(
    #         url=self.URL,
    #         headers=header_client,
    #     )
    #     assert response.status_code == 200, response.text
    #     records = Services.parse_obj(response.json()).records
    #     assert len(records) == 2
    #     assert records[0].name == 'cut hair'
    #     assert records[0].price == 400
    #     assert records[1].name == 'cut beard'
    #     assert records[1].price == 500
