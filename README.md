# Pearl Todd API

Generate description of project. python, API, for barbershop, models: master, client, schedule, services, booking

### How to run it
1. project use poetry
2. may use to generate secret key ```openssl rand -hex 32```
3. create file .env in project's root according to example [.env.example](.env.example)
4. From root: ```python3 run.py```
5. docker

Here are the general steps to run a Python project that uses FastAPI, Poetry, and SQLAlchemy:

1. Clone the project's Git repository. 
2. Install the [Poetry package manager](https://python-poetry.org/) on your system if you haven't already done so. 
3. Open a terminal window and navigate to the project's root directory. 
4. Run the command poetry install to install the required dependencies listed in the pyproject.toml file. 
5. Create a new database for the project (if required), and update the database configuration settings in the project's configuration files. 
6. Run the database migration command to create the necessary tables in the database. The exact command may vary depending on the specific database management system being used. 
7. Run the command poetry run uvicorn main:app --reload to start the FastAPI server. This will launch the server with automatic reloading enabled, which means any changes made to the code will automatically be reloaded by the server. 
8. Open a web browser and navigate to the URL of the running server to interact with the API. 
9. Note that these steps are general guidelines, and the exact commands and configuration settings may vary depending on the specific details of the project you are working with.
