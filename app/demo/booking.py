import app.model as models
from .demo_data import DemoData
from ..schemas.booking import BookingBase


class BookingDemo(DemoData):
    def __init__(self):
        super().__init__()
        self.modeldb = models.Booking
        self.schema = BookingBase
        self.path_demo = 'app/demo/src/booking.json'
        self.responses = "Bookings created", "Bookings DIDN'T created"
