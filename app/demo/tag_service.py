import app.model as models
from .demo_data import DemoData
from ..schemas.tag_service import TagServiceCreate


class TagServiceDemo(DemoData):
    def __init__(self):
        super().__init__()
        self.modeldb = models.TagService
        self.schema = TagServiceCreate
        self.path_demo = 'app/demo/src/tag_service.json'
        self.responses = "Tags service created", "Tags service DIDN'T created"
