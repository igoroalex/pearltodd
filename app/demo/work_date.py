import app.model as models
from .demo_data import DemoData
from ..schemas.work_date import WorkDateBase


class WorkDateDemo(DemoData):
    def __init__(self):
        super().__init__()
        self.modeldb = models.WorkDate
        self.schema = WorkDateBase
        self.path_demo = 'app/demo/src/work_date.json'
        self.responses = "Work dates created", "Work dates DIDN'T created"
