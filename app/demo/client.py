import app.model as models
from .demo_data import DemoData
from ..schemas.client import ClientBase


class ClientDemo(DemoData):
    def __init__(self):
        super().__init__()
        self.modeldb = models.Client
        self.schema = ClientBase
        self.path_demo = 'app/demo/src/client.json'
        self.responses = "Clients created", "Clients DIDN'T created"
