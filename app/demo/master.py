import app.model as models
from ..schemas.master import MasterCreate
from .demo_data import DemoData


class MasterDemo(DemoData):
    def __init__(self):
        super().__init__()
        self.modeldb = models.Master
        self.schema = MasterCreate
        self.path_demo = 'app/demo/src/master.json'
        self.responses = "Masters created", "Masters DIDN'T created"
