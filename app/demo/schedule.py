import app.model as models
from .demo_data import DemoData
from ..schemas.schedule import ScheduleBase


class ScheduleDemo(DemoData):
    def __init__(self):
        super().__init__()
        self.modeldb = models.Schedule
        self.schema = ScheduleBase
        self.path_demo = 'app/demo/src/schedule.json'
        self.responses = "Schedules created", "Schedules DIDN'T created"
