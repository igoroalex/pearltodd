import app.model as models
from .demo_data import DemoData
from ..schemas.service import ServiceBase


class ServiceDemo(DemoData):
    def __init__(self):
        super().__init__()
        self.modeldb = models.Service
        self.schema = ServiceBase
        self.path_demo = 'app/demo/src/service.json'
        self.responses = "Services created", "Services DIDN'T created"
