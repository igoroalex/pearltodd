import app.model as models
from .demo_data import DemoData
from ..schemas.workplace import WorkplaceCreate


class WorkplaceDemo(DemoData):

    def __init__(self):
        super().__init__()
        self.modeldb = models.Workplace
        self.schema = WorkplaceCreate
        self.path_demo = 'app/demo/src/workplace.json'
        self.responses = "Workplaces created", "Workplaces DIDN'T created"
