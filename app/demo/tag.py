import app.model as models
from .demo_data import DemoData
from ..schemas.tag import TagCreate


class TagDemo(DemoData):
    def __init__(self):
        super().__init__()
        self.modeldb = models.Tag
        self.schema = TagCreate
        self.path_demo = 'app/demo/src/tag.json'
        self.responses = "Tags created", "Tags DIDN'T created"
