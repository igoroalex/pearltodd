import json
import logging

from app.utils.engine import get_db

logger = logging.getLogger(__name__)


class DemoData:
    def __init__(self):
        self.modeldb = None
        self.schema = None
        self.path_demo = ''
        self.responses = "", ""

    def add_record(self, record):
        db = next(get_db())
        cur_model = self.get_schema()(**record)
        # cur_model = self.schemas(**record)

        cur_record = self.modeldb(**cur_model.dict())
        cur_record.post(db)
        return True

    def add_demo_data(self):
        demo_data = self.get_demo_data()
        is_success = all(map(self.add_record, demo_data))
        response = self.get_responses()[0] if is_success else self.get_responses()[1]
        logger.info(response)
        return is_success

    def get_responses(self) -> tuple:
        return self.responses

    def get_demo_data(self):
        return self.get_json_demo()

    def get_modeldb(self):
        return self.modeldb

    def get_schema(self):
        return self.schema

    def get_path_demo(self):
        return self.path_demo

    def get_json_demo(self):
        with open(self.get_path_demo(), 'r') as f:
            demo_data = json.load(f)
        return demo_data
