from fastapi import Depends, APIRouter
from sqlalchemy.orm import Session
from starlette.responses import JSONResponse

import app.model as models
from ..schemas.schedule import Schedule, ScheduleCreate, Schedules
from ..utils import get_db, CurrentMaster

router = APIRouter(
    prefix='/api/schedules',
    tags=['schedules'],
    responses={404: {'description': "No response from router /api/schedules"}},
)


@router.get('/', response_model=Schedules)
async def get_schedules(
    cur_master: CurrentMaster,
    db: Session = Depends(get_db),
):
    return {'records': models.Schedule.get_all(db, filters={'master_id': cur_master.id})}


@router.post('/', response_model=Schedule, status_code=201)
async def post_schedule(
    new_schedule: ScheduleCreate,
    cur_master: CurrentMaster,
    db: Session = Depends(get_db),
):
    new_schedule.master_id = cur_master.id
    record = models.Schedule(**new_schedule.dict())

    return {'record': record.post(db)}


@router.delete('/{schedule_id}', response_model=Schedule)
async def delete_schedule(
    schedule_id: int,
    cur_master: CurrentMaster,
    db: Session = Depends(get_db),
):
    if record := models.Schedule.get_first(db, filters={'master_id': cur_master.id, 'id': schedule_id}):
        return {'record': record.delete(db)}

    return JSONResponse(status_code=404, content={'detail': "No schedule found"})
