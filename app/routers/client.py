from fastapi import Depends, APIRouter
from sqlalchemy.orm import Session
from starlette.responses import JSONResponse

import app.model as models
from ..schemas.client import ClientCreate, Client, Clients
from ..utils import get_db, CurrentMaster

router = APIRouter(
    prefix='/api/clients',
    tags=['clients'],
    responses={404: {'description': "No response from router /api/clients"}},
)


@router.get('/', response_model=Clients)
async def get_clients(
    cur_master: CurrentMaster,
    search_data: str = None,
    workplace_id: int = None,
    db: Session = Depends(get_db),
):
    filters = {'master_id': cur_master.id}
    if search_data:
        filters.update({'search_data': search_data})
    if workplace_id:
        filters.update({'workplace_id': workplace_id})

    return {'records': models.Client.get_all(db, filters=filters)}


@router.post('/', response_model=Client, status_code=201)
async def post_client(
    new_client: ClientCreate,
    cur_master: CurrentMaster,
    db: Session = Depends(get_db),
):
    record = models.Client(**new_client.dict())

    return {'record': record.post(db)}


@router.delete('/{client_id}', response_model=Client)
async def delete_client(
    client_id: int,
    cur_master: CurrentMaster,
    db: Session = Depends(get_db),
):
    if record := models.Client.get_first(db, filters={'master_id': cur_master.id, 'id': client_id}):
        return {'record': record.delete(db)}

    return JSONResponse(status_code=404, content={'detail': "No client found"})
