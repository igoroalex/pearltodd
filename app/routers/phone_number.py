import phonenumbers
from fastapi import APIRouter
from starlette.responses import JSONResponse

from ..schemas.phone_number import PhoneNumber
from ..utils import CurrentMaster

router = APIRouter(
    prefix='/api/phone_number',
    tags=['phone_number'],
    responses={404: {'description': "No response from router /api/phone_number"}},
)


@router.get('/', response_model=PhoneNumber)
async def get_phone_number(
    cur_master: CurrentMaster,
    phone_number: str,
):
    if len(phone_number) == 9:
        phone_number = f"380{phone_number}"
    if len(phone_number) == 10:
        phone_number = f"38{phone_number}"

    val = phonenumbers.parse(phone_number, 'UA')
    if phonenumbers.is_valid_number(val):
        return PhoneNumber(phone_number=phonenumbers.format_number(val, phonenumbers.PhoneNumberFormat.E164))

    return JSONResponse(status_code=404, content={'detail': "phone number is not valid"})
