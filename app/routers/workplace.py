from fastapi import Depends, APIRouter
from sqlalchemy.orm import Session
from starlette.responses import JSONResponse

import app.model as models
from ..schemas.workplace import Workplace, WorkplaceCreate, Workplaces
from ..utils import get_db, CurrentMaster

router = APIRouter(
    prefix='/api/workplaces',
    tags=['workplaces'],
    responses={404: {'description': "No response from router /api/workplaces"}},
)


@router.get('/', response_model=Workplaces)
async def get_workplaces(
    cur_master: CurrentMaster,
    db: Session = Depends(get_db),
):
    return {'records': models.Workplace.get_all(db, filters={'master_id': cur_master.id})}


@router.post('/', response_model=Workplace, status_code=201)
async def post_workplace(
    new_workplace: WorkplaceCreate,
    cur_master: CurrentMaster,
    db: Session = Depends(get_db),
):
    new_workplace.master_id = cur_master.id
    record = models.Workplace(**new_workplace.dict())
    return {'record': record.post(db)}


@router.delete('/{workplace_id}', response_model=Workplace)
async def delete_workplace(
    workplace_id: int,
    cur_master: CurrentMaster,
    db: Session = Depends(get_db),
):
    if record := models.Workplace.get_first(db, filters={'master_id': cur_master.id, 'id': workplace_id}):
        return {'record': record.delete(db)}

    return JSONResponse(status_code=404, content={'detail': "No service found"})
