from typing import Annotated

from fastapi import Depends, APIRouter, Security
from sqlalchemy.orm import Session

import app.model as models
from ..schemas.master import Master, MasterCreate
from ..schemas.token_data import TokenData
from ..utils import get_checked_token_data, get_db, CurrentMaster

router = APIRouter(
    prefix='/api/masters',
    tags=['masters'],
    responses={404: {'description': "No response from router /api/masters"}},
)


@router.get('/master', response_model=Master)
async def get_master(
    cur_master: CurrentMaster,
):
    return {'record': cur_master}


@router.post('/', response_model=Master, status_code=201)
async def post_master(
    new_master: MasterCreate,
    token_data: Annotated[TokenData, Security(get_checked_token_data, scopes=['master'])],
    db: Session = Depends(get_db),
):
    record = models.Master(**new_master.dict())

    return {'record': record.post(db)}
