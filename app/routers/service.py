from typing import Annotated

from fastapi import Depends, APIRouter, Security
from sqlalchemy.orm import Session
from starlette.responses import JSONResponse

import app.model as models
from ..schemas.service import ServiceCreate, Service, Services, ServicePatch
from ..utils import get_db, get_active_user, CurrentMaster

router = APIRouter(
    prefix='/api/services',
    tags=['services'],
    responses={404: {'description': "No response from router /api/services"}},
)


@router.get('/', response_model=Services)
async def get_services(
    cur_master: Annotated[models.Master, Security(get_active_user, scopes=['master', 'client'])],
    workplace_id: int = None,
    db: Session = Depends(get_db),
):
    filters = {'master_id': cur_master.id}
    if workplace_id:
        filters.update({'workplace_id': workplace_id})
    return {'records': models.Service.get_all(db, filters=filters)}


@router.post('/', response_model=Service, status_code=201)
async def post_service(
    new_service: ServiceCreate,
    cur_master: CurrentMaster,
    db: Session = Depends(get_db),
):
    record = models.Service(**new_service.dict())

    return {'record': record.post(db)}


@router.patch('/{service_id}', response_model=Service)
async def patch_service(
    service_id: int,
    cur_service: ServicePatch,
    cur_master: CurrentMaster,
    db: Session = Depends(get_db),
):
    if record := models.Service.get_first(db, filters={'master_id': cur_master.id, 'id': service_id}):
        return {'record': record.patch(db, cur_service.dict())}

    return JSONResponse(status_code=404, content={'detail': "No services found"})


@router.delete('/{service_id}', response_model=Service)
async def delete_service(
    service_id: int,
    cur_master: CurrentMaster,
    db: Session = Depends(get_db),
):
    if record := models.Service.get_first(db, filters={'master_id': cur_master.id, 'id': service_id}):
        return {'record': record.delete(db)}

    return JSONResponse(status_code=404, content={'detail': "No service found"})
