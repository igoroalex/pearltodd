from typing import List

from fastapi import Depends, HTTPException, APIRouter
from sqlalchemy.orm import Session

import app.model as models
from ..schemas.tag import TagCreate, TagCreated, Tag
from ..utils import get_db, CurrentMaster

router = APIRouter(
    prefix='/api/tags',
    tags=['tags'],
    responses={404: {'description': "No response from router /api/services"}},
)


@router.get('/', response_model=List[Tag])
def get_tags(
    cur_master: CurrentMaster,
    name: str = None,
    db: Session = Depends(get_db),
):
    filters = dict()
    if name:
        filters['name'] = name
        if tag_db := models.Tag.get_all(db, filters=filters):
            return tag_db
        raise HTTPException(status_code=204, detail="No tag found")

    return models.Tag.get_all(db)


@router.post('/', response_model=TagCreated, status_code=201)
def post_tag(
    tag: TagCreate,
    cur_master: CurrentMaster,
    db: Session = Depends(get_db),
):
    db_tag = models.Tag(**tag.dict())
    return db_tag.post(db)
