from typing import List

from fastapi import Depends, APIRouter
from sqlalchemy.orm import Session

import app.model as models
from ..utils import get_db
from ..schemas.tag_service import TagService

router = APIRouter(
    prefix='/api/tag_services',
    tags=['tag_services'],
    responses={404: {'description': "No response from router /api/services"}},
)
