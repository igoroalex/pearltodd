from fastapi import Depends, APIRouter
from sqlalchemy.orm import Session
from starlette.responses import JSONResponse

import app.model as models
from ..schemas.booking import Booking, BookingCreate, Bookings
from ..utils import get_db, CurrentMaster

router = APIRouter(
    prefix='/api/bookings',
    tags=['bookings'],
    responses={404: {'description': "No response from router /api/bookings"}},
)


@router.get('/', response_model=Bookings)
async def get_bookings(
    cur_master: CurrentMaster,
    db: Session = Depends(get_db),
):
    return {'records': models.Booking.get_all(db, filters={'master_id': cur_master.id})}


@router.post('/', response_model=Booking, status_code=201)
async def post_booking(
    cur_master: CurrentMaster,
    new_booking: BookingCreate,
    db: Session = Depends(get_db),
):
    record = models.Booking(**new_booking.dict())

    return {'record': record.post(db)}


@router.get('/booking/', response_model=Booking)
async def get_booking(
    cur_master: CurrentMaster,
    booking_id: int,
    db: Session = Depends(get_db),
):
    if record := models.Booking.get_first(db, filters={'id': booking_id}):
        return record

    return JSONResponse(status_code=404, content={'detail': "No booking found"})


@router.delete('/{booking_id}', response_model=Booking)
async def delete_booking(
    booking_id: int,
    cur_master: CurrentMaster,
    db: Session = Depends(get_db),
):
    if record := models.Booking.get_first(db, filters={'master_id': cur_master.id, 'id': booking_id}):
        return {'record': record.delete(db)}

    return JSONResponse(status_code=404, content={'detail': "No booking found"})


# async def master_date(schedule, bookings, current_date):
#     """Get Master's booking on date"""
#     bookings = [Booking.from_orm(i) for i in bookings]
#
#     is_booking = lambda t: not any(True for i in bookings if i.start_datetime <= t.start_datetime < i.end_datetime)
#     bookings += list(filter(is_booking, get_schedule_30(schedule, current_date)))
#     bookings.sort(key=lambda i: i.start_datetime)
#
#     bookings = [jsonable_encoder(i) for i in bookings]
#     return JSONResponse(status_code=200, content=bookings)


# async def get_schedule_30(schedule, current_date: date):
#     """Get Master's schedule on date"""
#     start_datetime = datetime.combine(current_date, schedule.start_time, timezone.utc)
#     end_datetime = datetime.combine(current_date, schedule.end_time, timezone.utc)
#     from_to_hour = range(0, (end_datetime.hour - start_datetime.hour) * 60, 30)
#     return [ZeroBooking(start_datetime=start_datetime + timedelta(minutes=i)) for i in from_to_hour]


# @router.get('/date/')
# async def get_bookings_by_date(
#     master_ext_id: str,
#     current_date: date,
#     db: Session = Depends(get_db),
# ):
#     """Get Master's booking on date"""
#     schedule = models.Schedule.get_first(db, filters={'master_ext_id': master_ext_id})
#     filters = {'master_ext_id': master_ext_id, 'current_date': current_date}
#     bookings = models.Booking.get_all(db, filters=filters)
#
#     return master_date(schedule, bookings, current_date)


# @router.get('/dates/', response_model=List[BookingDate])
# async def get_bookings_all_dates(
#     master_ext_id: str,
#     db: Session = Depends(get_db),
# ):
#     """Get all Master's booking"""
#     if master_ext_id:
#         return models.Booking.get_all(db, filters={'master_ext_id': master_ext_id})
#
#     return models.Booking.get_all(db)
