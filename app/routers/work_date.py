from fastapi import Depends, APIRouter
from sqlalchemy.orm import Session
from starlette.responses import JSONResponse

import app.model as models
from ..schemas.work_date import WorkDate, WorkDateCreate, WorkDates
from ..utils import get_db, CurrentMaster

router = APIRouter(
    prefix='/api/work_dates',
    tags=['work_dates'],
    responses={404: {'description': "No response from router /api/work_dates"}},
)


@router.get('/', response_model=WorkDates)
async def get_work_dates(
    cur_master: CurrentMaster,
    workplace_id: int = None,
    is_active: int = True,
    db: Session = Depends(get_db),
):
    filters = {'master_id': cur_master.id}
    if workplace_id:
        filters.update({'workplace_id': workplace_id})
    if is_active:
        filters.update({'is_active': is_active})
    return {'records': models.WorkDate.get_all(db, filters=filters)}


@router.post('/', response_model=WorkDate, status_code=201)
async def post_work_date(
    new_work_date: WorkDateCreate,
    cur_master: CurrentMaster,
    db: Session = Depends(get_db),
):
    if work_date := models.WorkDate.get_first(
        db,
        filters={
            'master_id': cur_master.id,
            'workplace_id': new_work_date.workplace_id,
            'schedule_id': new_work_date.schedule_id,
            'date': new_work_date.date,
        },
    ):
        return {'record': work_date.patch(db, {'is_active': True})}

    record = models.WorkDate(**new_work_date.dict())

    return {'record': record.post(db)}


@router.delete('/{work_date_id}', response_model=WorkDate)
async def delete_work_date(
    work_date_id: int,
    cur_master: CurrentMaster,
    db: Session = Depends(get_db),
):
    if record := models.WorkDate.get_first(db, filters={'master_id': cur_master.id, 'id': work_date_id}):
        return {'record': record.delete(db)}

    return JSONResponse(status_code=404, content={'detail': "No work_date found"})
