from fastapi import HTTPException
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session


class ModelDB:
    def __init__(self):
        self.is_active = None

    @classmethod
    def _use_active(cls, query):
        return query.filter_by(**{'is_active': True})

    @classmethod
    def _use_filters(cls, query, filters):
        filters = filters if filters else {}

        return query.filter_by(**filters)

    @classmethod
    def _use_order(cls, query):
        return query

    @classmethod
    def _get_query(cls, db: Session, filters):
        query = db.query(cls)
        query = cls._use_active(query)
        query = cls._use_filters(query, filters)
        query = cls._use_order(query)

        return query

    @classmethod
    def get_all(cls, db: Session, filters: dict = None, skip: int = 0, limit: int = 100):
        query = cls._get_query(db, filters)
        query = query.offset(skip).limit(limit)

        return query.all()

    @classmethod
    def get_first(cls, db: Session, filters: dict = None):
        query = cls._get_query(db, filters)

        return query.first()

    def check_relationship(self):
        return True

    def post(self, db: Session, add_new: bool = True):
        if add_new:
            db.add(self)

        try:
            db.commit()

            self.check_relationship()

        except IntegrityError as err:
            db.rollback()
            raise HTTPException(status_code=404, detail="Cannot create record") from err

        except HTTPException as err:
            db.rollback()
            raise err

        if add_new:
            db.refresh(self)

        return self

    def patch(self, db: Session, data: dict):
        for key, value in data.items():
            if hasattr(self, key) and value and getattr(self, key) != value:
                setattr(self, key, value)

        return self.post(db, add_new=False)

    def delete(self, db: Session):
        self.is_active = False

        return self.post(db, add_new=False)
