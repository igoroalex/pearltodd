from sqlalchemy import Column, String, Integer, Boolean, UniqueConstraint
from sqlalchemy.orm import relationship

from .model_db import ModelDB
from ..utils.engine import Base


class Master(Base, ModelDB):
    __tablename__ = 'masters'

    id = Column(Integer, primary_key=True)
    is_active = Column(Boolean, default=True)

    full_name = Column(String(128), nullable=False)
    telegram_id = Column(String(128), index=True)
    username = Column(String(128), nullable=False)

    workplaces = relationship('Workplace', back_populates='master')
    schedules = relationship('Schedule', back_populates='master')

    __table_args__ = (UniqueConstraint('telegram_id', name='_telegram_uc'),)

# TODO add table users
