from datetime import date

from fastapi import HTTPException
from sqlalchemy import Column, Integer, Date, ForeignKey, Boolean, UniqueConstraint
from sqlalchemy.orm import relationship

from .master import Master
from .model_db import ModelDB
from .workplace import Workplace
from ..utils.engine import Base


class WorkDate(Base, ModelDB):
    __tablename__ = 'work_dates'

    id = Column(Integer, primary_key=True)
    is_active = Column(Boolean, default=True)

    date = Column(Date, nullable=False, index=True)

    workplace_id = Column(Integer, ForeignKey('workplaces.id'), nullable=False, index=True)
    workplace = relationship('Workplace', back_populates='work_dates')

    schedule_id = Column(Integer, ForeignKey('schedules.id'), nullable=False)
    schedule = relationship('Schedule', back_populates='work_dates')

    __table_args__ = (UniqueConstraint('date', 'workplace_id', name='_date_workplace_uc'),)

    @classmethod
    def _use_active(cls, query):
        return query

    @classmethod
    def _use_filters(cls, query, filters: dict = None):
        filters = filters if filters else dict()

        query = query.filter(cls.date >= date.today())

        if master_id := filters.get('master_id', None):
            query = query.filter(cls.workplace.has(Workplace.master.has(Master.id == master_id)))
            filters.pop('master_id')

        return query.filter_by(**filters)

    def check_relationship(self):
        if not self.workplace:
            raise HTTPException(status_code=404, detail="No workplace found")
        if not self.schedule:
            raise HTTPException(status_code=404, detail="No schedule found")
