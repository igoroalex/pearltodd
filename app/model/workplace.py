from fastapi import HTTPException
from sqlalchemy import Column, String, Integer, ForeignKey, Boolean, UniqueConstraint
from sqlalchemy.orm import relationship

from .model_db import ModelDB
from ..utils.engine import Base


class Workplace(Base, ModelDB):
    __tablename__ = 'workplaces'

    id = Column(Integer, primary_key=True)
    is_active = Column(Boolean, default=True)

    master_id = Column(Integer, ForeignKey('masters.id'), nullable=False, index=True)
    master = relationship('Master')

    services = relationship('Service', back_populates='workplace')
    clients = relationship('Client', back_populates='workplace')
    work_dates = relationship('WorkDate', back_populates='workplace')

    address = Column(String(256), nullable=False)

    __table_args__ = (UniqueConstraint('master_id', 'address', name='_master_address_uc'),)

    def check_relationship(self):
        if not self.master:
            raise HTTPException(status_code=404, detail="No master found")
