from sqlalchemy import Column, String, Integer, Boolean, UniqueConstraint

from .model_db import ModelDB
from ..utils.engine import Base


class Tag(Base, ModelDB):
    __tablename__ = 'tags'

    id = Column(Integer, primary_key=True)
    is_active = Column(Boolean, default=True)

    name = Column(String(128), nullable=False, index=True)

    __table_args__ = (UniqueConstraint('name', name='name_uc'),)
