from fastapi import HTTPException
from sqlalchemy import Column, String, Integer, ForeignKey, Boolean, UniqueConstraint
from sqlalchemy.orm import relationship

from .master import Master
from .model_db import ModelDB
from .workplace import Workplace
from ..utils.engine import Base


class Client(Base, ModelDB):
    __tablename__ = 'clients'

    id = Column(Integer, primary_key=True)
    is_active = Column(Boolean, default=True)

    name = Column(String(128))
    telegram_id = Column(String(128), index=True)
    phone_number = Column(String(20))

    workplace_id = Column(Integer, ForeignKey('workplaces.id'), nullable=False, index=True)
    workplace = relationship('Workplace', back_populates='clients')

    __table_args__ = (UniqueConstraint('workplace_id', 'telegram_id', name='_workplace_telegram_uc'),)

    @classmethod
    def _use_filters(cls, query, filters: dict = None):
        filters = filters if filters else dict()

        if master_id := filters.get('master_id', None):
            query = query.filter(cls.workplace.has(Workplace.master.has(Master.id == master_id)))
            filters.pop('master_id')

        if search_data := filters.get('search_data', None):
            query = query.filter(cls.name.contains(search_data) | cls.phone_number.contains(search_data))
            filters.pop('search_data')

        return query.filter_by(**filters)

    def check_relationship(self):
        if not self.workplace:
            raise HTTPException(status_code=404, detail="No workplace found")
