from datetime import date, datetime, time

from fastapi import HTTPException
from sqlalchemy import Column, String, Integer, DateTime, ForeignKey, Boolean, UniqueConstraint
from sqlalchemy.orm import relationship

from .master import Master
from .model_db import ModelDB
from .workplace import Workplace
from ..utils.engine import Base


class Booking(Base, ModelDB):
    __tablename__ = 'bookings'

    id = Column(Integer, primary_key=True, index=True)

    start_datetime = Column(DateTime, nullable=False, index=True)

    workplace_id = Column(Integer, ForeignKey('workplaces.id'), nullable=False, index=True)
    workplace = relationship('Workplace')

    client_id = Column(Integer, ForeignKey('clients.id'), nullable=False)
    client = relationship('Client')

    service_id = Column(Integer, ForeignKey('services.id'), nullable=False)
    service = relationship('Service')

    status = Column(String(32))
    comment = Column(String(256))
    is_active = Column(Boolean, default=True)

    __table_args__ = (
        UniqueConstraint('client_id', 'start_datetime', name='_client_start_datetime_uc'),
        UniqueConstraint('workplace_id', 'start_datetime', name='_workplace_start_datetime_uc'),
    )

    @classmethod
    def _use_filters(cls, query, filters: dict = None):
        filters = filters if filters else {}

        if master_id := filters.get('master_id', None):
            query = query.filter(cls.start_datetime > date.today())
            query = query.filter(cls.workplace.has(Workplace.master.has(Master.id == master_id)))
            filters.pop('master_id')

        if current_date := filters.get('current_date', None):
            query = query.filter(cls.start_datetime > datetime.combine(current_date, time(0, 0, 0)))
            query = query.filter(cls.start_datetime < datetime.combine(current_date, time(23, 59, 59)))
            filters.pop('current_date')

        return query.filter_by(**filters)

    @classmethod
    def _use_order(cls, query):
        return query.order_by(cls.start_datetime)

    def check_relationship(self):
        if not self.workplace:
            raise HTTPException(status_code=404, detail="No workplace found")
        if not self.client:
            raise HTTPException(status_code=404, detail="No client found")
        if not self.service:
            raise HTTPException(status_code=404, detail="No service found")
