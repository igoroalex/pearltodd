from fastapi import HTTPException
from sqlalchemy import Column, String, Integer, ForeignKey, Boolean, UniqueConstraint
from sqlalchemy.orm import relationship, Session

from .master import Master
from .model_db import ModelDB
from .workplace import Workplace
from ..utils.engine import Base


class Service(Base, ModelDB):
    __tablename__ = 'services'

    id = Column(Integer, primary_key=True)
    is_active = Column(Boolean, default=True)

    name = Column(String(128), nullable=False)

    workplace_id = Column(Integer, ForeignKey('workplaces.id'), nullable=False, index=True)
    workplace = relationship('Workplace', back_populates='services')

    duration = Column(Integer, nullable=False)
    rest_time = Column(Integer)
    price = Column(Integer)
    remind = Column(Boolean)
    description = Column(String(128))
    comment = Column(String(128))

    __table_args__ = (UniqueConstraint('workplace_id', 'name', 'price', name='_workplace_name_price_uc'),)

    @classmethod
    def _use_filters(cls, query, filters: dict = None):
        filters = filters if filters else dict()

        if master_id := filters.get('master_id', None):
            query = query.filter(cls.workplace.has(Workplace.master.has(Master.id == master_id)))
            filters.pop('master_id')

        return query.filter_by(**filters)

    def check_relationship(self):
        if not self.workplace:
            raise HTTPException(status_code=404, detail="No workplace found")

# query = query.filter(cls.movie_session.has(
#     or_(models.Movie.title == search_for_filter,
#         models.Movie.genres.any(models.Genre.name.contains(search_for_filter)),
#         models.Movie.cast.any(models.Person.name.contains(search_for_filter)),
#         models.Movie.directors.any(models.Person.name.contains(search_for_filter)),
#         )))
#
# subquery = db.query(cls.movie_id.label('movie_id'), func.count(models.Ticket.id).label('count_tickets')) \
#     .join(models.Ticket, models.Ticket.session_movie_id == cls.id) \
#     .filter_by(**{'is_active': True}) \
#     .group_by(cls.movie_id) \
#     .order_by(cls.movie_id) \
#     .subquery()
#
# query = db.query(models.Movie, subquery.c.movie_id, subquery.c.count_tickets) \
#     .join(models.Movie, subquery.c.movie_id == models.Movie.id) \
#     .order_by(subquery.c.movie_id)
