from fastapi import HTTPException
from sqlalchemy import (
    Column,
    Integer,
    Time,
    ForeignKey,
    Boolean,
    UniqueConstraint,
)
from sqlalchemy.orm import relationship

from .model_db import ModelDB
from ..utils.engine import Base


class Schedule(Base, ModelDB):
    __tablename__ = 'schedules'

    id = Column(Integer, primary_key=True)
    is_active = Column(Boolean, default=True)

    master_id = Column(Integer, ForeignKey('masters.id'), nullable=False, index=True)
    master = relationship('Master')

    work_dates = relationship('WorkDate', back_populates='schedule')

    start_time = Column(Time, nullable=False)
    end_time = Column(Time, nullable=False)

    __table_args__ = (
        UniqueConstraint(
            'master_id',
            'start_time',
            'end_time',
            name='_master_start_time_end_time_uc',
        ),
    )

    def check_relationship(self):
        if not self.master:
            raise HTTPException(status_code=404, detail="No master found")
