from sqlalchemy import Column, Integer, ForeignKey, Boolean, UniqueConstraint
from sqlalchemy.orm import relationship

from .model_db import ModelDB
from ..utils.engine import Base


class TagService(Base, ModelDB):

    __tablename__ = 'tags_service'

    id = Column(Integer, primary_key=True)
    is_active = Column(Boolean, default=True)

    service_id = Column(Integer, ForeignKey('services.id'), nullable=False, index=True)
    service = relationship('Service')

    tag_id = Column(Integer, ForeignKey('tags.id'), nullable=False, index=True)
    tag = relationship('Tag')

    __table_args__ = (UniqueConstraint('tag_id', 'service_id', name='_tag_service_uc'),)
