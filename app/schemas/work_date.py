from datetime import date
from typing import List, Optional

from app.schemas.base_orm import BaseORM


class WorkDateBase(BaseORM):
    id: int
    date: date
    workplace_id: int
    schedule_id: int
    is_active: bool | None


class WorkDateCreate(WorkDateBase):
    id: Optional[int] = None


class WorkDate(BaseORM):
    record: WorkDateBase


class WorkDates(BaseORM):
    records: List[WorkDateBase]
