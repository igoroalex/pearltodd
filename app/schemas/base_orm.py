from typing import List

from pydantic import BaseModel


class BaseORM(BaseModel):
    class Config:
        orm_mode = True


class ResponseDetail(BaseModel):
    detail: str


class ResponseLoc(BaseModel):
    loc: List[str]
    msg: str
    type: str


class ResponseDetailLoc(BaseModel):
    detail: List[ResponseLoc]
