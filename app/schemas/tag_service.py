from pydantic import BaseModel

from .base_orm import BaseORM


class TagServiceCreate(BaseModel):
    service_id: int
    tag_id: int


class TagServiceCreated(BaseORM):
    id: int


class TagService(
    TagServiceCreate,
    TagServiceCreated,
):
    pass


class TagServicePatch(TagServiceCreate):
    pass
