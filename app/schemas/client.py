from typing import List, Optional

from app.schemas.base_orm import BaseORM


class ClientBase(BaseORM):
    id: int
    name: str
    workplace_id: int
    phone_number: Optional[str] = None
    telegram_id: str


class ClientCreate(ClientBase):
    id: Optional[int] = None


class Client(BaseORM):
    record: ClientBase


class Clients(BaseORM):
    records: List[ClientBase]
