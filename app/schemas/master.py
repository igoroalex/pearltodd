from typing import List

from pydantic import BaseModel

from app.schemas.base_orm import BaseORM


class MasterBase(BaseORM):
    id: int
    full_name: str
    username: str


class MasterCreate(BaseModel):
    telegram_id: str
    full_name: str
    username: str


class Master(BaseORM):
    record: MasterBase


class Masters(BaseModel):
    records: List[Master]
