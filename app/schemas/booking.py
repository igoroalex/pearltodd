from typing import List, Optional

from pydantic.datetime_parse import datetime

from app.schemas.base_orm import BaseORM


class BookingBase(BaseORM):
    client_id: int
    workplace_id: int
    service_id: int
    start_datetime: datetime
    comment: Optional[str] = None


class BookingCreate(BookingBase):
    id: Optional[int] = None


class Booking(BaseORM):
    record: BookingBase


class Bookings(BaseORM):
    records: List[BookingBase]


# class ServiceBooking(BaseORM):
#     name: str
#     duration: int
#     rest_time: int = 0
#     comment: str = ""
#
#
# class ClientBooking(BaseORM):
#     name: str
#     telegram_id: str
#
#
# class ZeroBooking(BaseModel):
#     start_datetime: datetime
#     zero: bool = True
#
#
# class Booking(BaseORM):
#     id: int
#     client: ClientBooking
#     service_name: str = ''
#     client_name: str = ''
#     start_datetime: datetime
#     end_datetime: Optional[datetime] = None
#     start_end_datetime: str = ''
#     service: ServiceBooking
#     comment: str = ''
#     zero: bool = False
#
#     @root_validator
#     def get_end_datetime(cls, values):
#         values['end_datetime'] = (
#             values['start_datetime']
#             + timedelta(minutes=values['service'].duration)
#             + timedelta(minutes=values['service'].rest_time)
#         )
#         return values
#
#     @root_validator
#     def set_start_end_datetime(cls, values):
#         values['start_datetime'] = values['start_datetime'].replace(tzinfo=timezone.utc)
#         values['end_datetime'] = values['end_datetime'].replace(tzinfo=timezone.utc)
#         values[
#             'start_end_datetime'
#         ] = f"{values['start_datetime'].strftime('%H:%M')} - {values['end_datetime'].strftime('%H:%M')}"
#         return values
#
#     @root_validator
#     def get_service_name(cls, values):
#         values['service_name'] = values['service'].name
#         return values
#
#     @root_validator
#     def get_service_comment(cls, values):
#         values['service_comment'] = values['service'].comment
#         return values
#
#     @root_validator
#     def get_client_name(cls, values):
#         values['client_name'] = values['client'].name
#         return values
#
#
# class BookingDate(BaseORM):
#     start_datetime: date
