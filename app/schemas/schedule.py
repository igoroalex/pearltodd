from datetime import time
from typing import List, Optional

from app.schemas.base_orm import BaseORM


class ScheduleBase(BaseORM):
    id: int
    master_id: int
    start_time: time
    end_time: time


class ScheduleCreate(ScheduleBase):
    id: Optional[int] = None
    master_id: Optional[int] = None


class Schedule(BaseORM):
    record: ScheduleBase


class Schedules(BaseORM):
    records: List[ScheduleBase]
