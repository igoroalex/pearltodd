from typing import List, Optional

from app.schemas.base_orm import BaseORM


class WorkplaceBase(BaseORM):
    id: int
    master_id: int
    address: str


class WorkplaceCreate(WorkplaceBase):
    id: Optional[int] = None
    master_id: Optional[int] = None


class Workplace(BaseORM):
    record: WorkplaceBase


class Workplaces(BaseORM):
    records: List[WorkplaceBase]
