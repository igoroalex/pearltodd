from typing import List, Optional

from pydantic import BaseModel

from app.schemas.base_orm import BaseORM


class ServiceBase(BaseORM):
    id: int
    workplace_id: int
    name: str
    duration: int
    rest_time: Optional[int] = None
    price: int
    description: Optional[str] = None
    comment: Optional[str] = None
    is_active: bool | None


class ServiceCreate(ServiceBase):
    id: Optional[int] = None


class Service(BaseORM):
    record: ServiceBase


class Services(BaseORM):
    records: List[ServiceBase]


class ServicePatch(BaseModel):
    name: Optional[str] = None
    duration: Optional[int] = None
    rest_time: Optional[int] = None
    price: Optional[int] = None
    description: Optional[str] = None
    comment: Optional[str] = None
