from datetime import datetime, timezone
from typing import List, Optional

from jose import jwt
from pydantic import BaseModel, root_validator

from app.utils.config import settings


class TokenData(BaseModel):
    user_ext_id: str
    scopes: List[str] = []
    date_time: datetime


class Token(BaseModel):
    user_ext_id: str
    access_token: str = ''
    scope: List[str]
    date_time: Optional[datetime] = None

    @root_validator
    def compute_access_token(cls, values):
        to_encode = {
            "user_ext_id": values['user_ext_id'],
            "scopes": values['scope'],
            "date_time": datetime.now(tz=timezone.utc).isoformat(),
        }
        values['access_token'] = jwt.encode(to_encode, settings.SECRET_KEY, algorithm=settings.ALGORITHM)
        return values
