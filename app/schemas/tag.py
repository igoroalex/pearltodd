from pydantic import BaseModel

from app.schemas.base_orm import BaseORM


class TagCreate(BaseModel):
    name: str


class TagCreated(BaseORM):
    id: int


class Tag(
    TagCreate,
    TagCreated,
):
    pass


class TagPatch(TagCreate):
    pass
