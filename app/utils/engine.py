from sqlalchemy import create_engine
from sqlalchemy.engine import Engine
from sqlalchemy.orm import sessionmaker, declarative_base

from .config import settings


def create_async_engine() -> Engine:
    return create_engine(url=settings.SQLALCHEMY_DATABASE_URL, connect_args={'check_same_thread': False})


def create_session_maker() -> sessionmaker:
    return sessionmaker(bind=engine, autoflush=False, autocommit=False)
    # return sessionmaker(bind=engine, class_=AsyncSession, autoflush=False, autocommit=False)


def create_metadata():
    Base.metadata.create_all(bind=engine)


def drop_metadata():
    Base.metadata.drop_all(bind=engine)


engine = create_async_engine()

SessionLocal = create_session_maker()

Base = declarative_base()

create_metadata()


def get_db() -> sessionmaker:
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
