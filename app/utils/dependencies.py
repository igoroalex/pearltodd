from datetime import datetime, timezone, timedelta
from typing import Annotated

from fastapi import Depends, HTTPException, status, Security
from fastapi.security import OAuth2PasswordBearer, SecurityScopes
from jose import jwt, JWTError
from pydantic import ValidationError
from sqlalchemy.orm import Session

from .config import settings
from .engine import get_db
from ..model.master import Master
from ..schemas.token_data import TokenData

oauth2_scheme = OAuth2PasswordBearer(
    tokenUrl='token',
    scopes={
        'master': "Read information about the current master",
        'admin': "admin root",
        'client': "Read for client",
    },
)


# TODO library fastapi_users
#  https://github1s.com/artemonsh/fastapi_course/blob/main/Lesson_09/src/auth/base_config.py#L27


def _get_user(
    security_scopes: SecurityScopes,
    token: Annotated[str, Depends(oauth2_scheme)],
    db: Annotated[Session, Depends(get_db)],
) -> Master:
    token_data = get_checked_token_data(security_scopes, token)

    cur_master = Master.get_first(db, filters={'telegram_id': token_data.user_ext_id})

    if not cur_master:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Not authenticated",
            headers={"WWW-Authenticate": f'Bearer scope="{security_scopes.scope_str}"'},
        )

    return cur_master


def get_checked_token_data(
    security_scopes: SecurityScopes,
    token: Annotated[str, Depends(oauth2_scheme)],
) -> TokenData:
    try:
        token_data = _get_token_data(token)

        _check_date_time(token_data)
        _check_scopes(security_scopes, token_data)

        return token_data

    except (JWTError, ValidationError):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Could not validate credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )


def get_active_user(current_user: Annotated[Master, Security(_get_user, scopes=['master'])]):
    if not current_user.is_active:
        raise HTTPException(status_code=400, detail="Inactive user")

    return current_user


CurrentMaster = Annotated[Master, Security(get_active_user, scopes=['master'])]


def _get_token_data(token: str) -> TokenData:
    try:
        payload = jwt.decode(token, settings.SECRET_KEY, algorithms=settings.ALGORITHM)

        return TokenData(**payload)

    except (JWTError, ValidationError):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Could not validate credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )


def _check_date_time(token_data: TokenData):
    if (datetime.now(tz=timezone.utc) - token_data.date_time) > timedelta(settings.ACCESS_TOKEN_EXPIRE_MINUTES):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Not enough permissions by date",
            headers={"WWW-Authenticate": f'Date time {token_data.date_time}'},
        )


def _check_scopes(security_scopes: SecurityScopes, token_data: TokenData):
    for scope in token_data.scopes:
        if scope not in security_scopes.scopes:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Not enough permissions",
                headers={"WWW-Authenticate": f'Bearer scope="{security_scopes.scope_str}"'},
            )
