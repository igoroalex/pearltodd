import logging

from pydantic import BaseSettings

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s %(levelname)s %(name)s: %(message)s",
)


class Settings(BaseSettings):
    APP_NAME: str = "PearlToddAPI"
    DESCRIPTION: str = "PearlTodd API helps CRUD instances. 🚀"
    HOST: str
    PORT: int
    RELOAD: bool
    SQLALCHEMY_DATABASE_URL: str
    SECRET_KEY: str
    ALGORITHM: str
    ACCESS_TOKEN_EXPIRE_MINUTES: int
    SUPER_ADMIN_ID: int

    class Config:
        env_file = '.env'


settings = Settings()
