import logging

import app.demo as demos
from . import create_metadata, drop_metadata

logger = logging.getLogger(__name__)


def zero_base():
    drop_metadata()
    create_metadata()

    demo_data = [
        demos.MasterDemo(),
        demos.WorkplaceDemo(),
        # demos.ScheduleDemo(),
        # demos.WorkDateDemo(),
        # demos.ClientDemo(),
        # demos.ServiceDemo(),
        # demos.BookingDemo(),
        # demos.TagDemo(),
        # demos.TagServiceDemo(),
    ]

    is_success = all(map(lambda dd: dd.add_demo_data(), demo_data))
    if is_success:
        logger.info("CREATED")
    else:
        logger.warning("DIDN'T created")


if __name__ == '__main__':
    zero_base()
