from fastapi import FastAPI

import app.routers as api
from app.utils.config import settings

app = FastAPI()


# TODO background tasks https://www.youtube.com/watch?v=fm4LTvMyiwE&list=PLeLN0qH0-mCVQKZ8-W1LhxDcVlWtTALCS&index=10
# TODO info https://www.youtube.com/watch?v=UkwpJyvf8CA
@app.on_event("startup")
def startup_event():
    configure_app()
    configure_api()


def configure_app():
    app.title = settings.APP_NAME
    app.description = settings.DESCRIPTION
    app.version = "0.0.2"
    # app.openapi_tags = get_tags_metadata()
    app.contact = {
        'name': "Oleksii Horodnychyi",
        'email': "igoroalex@gmail.com",
    }


def configure_api():
    routers = [
        api.master_router,
        api.booking_router,
        api.workplace_router,
        api.schedule_router,
        api.work_date_router,
        api.client_router,
        api.service_router,
        # api.tag_router,
        # api.tag_service_router,
        api.phone_number_router,
    ]
    added_routers = map(app.include_router, routers)
    if any(added_routers):
        pass


def get_tags_metadata():
    tags_metadata = [
        {
            'name': "sessions movie",
            'description': "Operations with session hall.",
        },
    ]

    return tags_metadata
